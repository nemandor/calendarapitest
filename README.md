Calculate deadline with help of Google Calendar API
=
This Maven project helps calculate the deadline when the start date and the working days are given as inputs.
The project calls a Google Calendar API which contains the holidays therefore the calculation can consider if a holiday event is on a weekday, so that day should be excluded from the calculation.
The calculation calculates with those working days which are on the weekend.

# Concept
Inputs: start_date, number of working days (working_days)
1) Calculate End_date: Working_days is added to the start_date
2) Count how many extra_days are between start and end date. Extra_days are the weekend days and public holidays.
3) If extra_days > 0 the End_date becomes the start_date and the extra_days are added to the start_days.
4) Previous steps are repeated till the extra_days=0;

##### *For example:*

- Start_date: 2019-04-30
- Working_days: 4


    End_date: Start_date + Working_days = 2019-05-04 
    Extra_days: 2 = 1 public holiday* + 1 weekend day (2019-05-04 = Saturday)
        
        *1st of May is public holiday in Hungary
2nd phase:
- Start_date: 2019-05-04
- Extra_days: 2


    End_date: Start_date + Extra_days = 2019-05-06    
    Extra days: 1 = 0 public holiday + 1 weekend day (2019-05-05 = Sunday)
3rd phase:
- Start_date: 2019-05-06
- Extra_days: 1


    End_date: Start_date + Extra_days = 2019-05-07    
    Extra days: 0 = 0 public holiday + 0 weekend day
>Output: 2019-05-07

# Setup

## Before running
1. You have to create a Google Calendar API and get the **Client ID** and **Client Secret**. You can find a detailed [tutorial on weblizar.com](https://weblizar.com/blog/get-google-calendar-client-id-and-client-secret-key/).
2. The **Client ID**, **Client Secret** and the **Project ID** has to be given in **credentials.json**
    ```json
    {
      "installed": {
        "client_id": "<<client_id>>",
        "project_id": "<<project_id>>",
        "client_secret": "<<client_secret>>",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://www.googleapis.com/oauth2/v3/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "redirect_uris": [
          "urn:ietf:wg:oauth:2.0:oob",
          "http://localhost"
        ]
      }
     } 
    ```
## At the first run
   At the first run of the project credential file called **StoredCredential** will be generated in *tokens* folder after you login Google and allow Quickstart. When it is finished you'll get this message in browser:
            
   >Received verification code. You may now close this window.

## Change Calendar
By changing the CALENDER_ID you can reach calendar of other countries, or your primary calendar.

*Primary calendar:*
```
    private static final String CALENDAR_ID="primary";
``` 
*Hungarian Holidays Google Calendar:*
```
    private static final String CALENDAR_ID="hu.hungarian#holiday@group.v.calendar.google.com";
``` 
You can find list of Google calendars in [mattn's github project](https://gist.github.com/mattn/1438183), or you can add manually to your calendar as in [this article.](https://support.google.com/calendar/answer/6084659?co=GENIE.Platform%3DAndroid&hl=en&oco=0)

# References
Get Started with the Calendar API [Java Quickstart](https://developers.google.com/calendar/quickstart/java) 