package hu.nemeth;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class WorkingDayCalculator {

    final static Logger logger = Logger.getLogger(WorkingDayCalculator.class);

    private static final String CALENDAR_ID="hu.hungarian#holiday@group.v.calendar.google.com";
    private Calendar service = new GoogleCalendarApi().getService();

    //List of the events which are not public holiday
    private static final List<String> HOLIDAY_EXCEPTIONS= Arrays.asList("New Year's Eve","Télapó","Mikulás", "Szilveszter");

    public LocalDate getEndDate(LocalDate startDate, int workDays){
        LocalDate localDate = startDate;
        int days = workDays;
        int extraDays=0;

        do {
            extraDays= countWeekendDays(localDate,days) + getNumberOfExtraHolidays(localDate,localDate.plusDays(days));
            localDate=localDate.plusDays(days);
            days=extraDays;
        }while(days!=0);

        return localDate;
    }

    private int countWeekendDays(LocalDate date, int days){
        int numberOfWeeks= days/7;
        int weekendDays=numberOfWeeks*2;
        date=date.plusDays(numberOfWeeks*7);
        for (int i=0; i<days-numberOfWeeks*7; i++){
            date=date.plusDays(1);
            if (date.getDayOfWeek()==DayOfWeek.SATURDAY || date.getDayOfWeek()==DayOfWeek.SUNDAY){
                weekendDays++;
            }
        }
        return weekendDays;
    }

    private int getNumberOfExtraHolidays(LocalDate startDate, LocalDate endDate){

        int numberOfHolidays=0;

        ZonedDateTime st = ZonedDateTime.of(startDate.plusDays(1), LocalTime.MIN, ZoneId.systemDefault());
        ZonedDateTime et = ZonedDateTime.of(endDate, LocalTime.MAX, ZoneId.systemDefault());


        try {
            Events events = service.events().list(CALENDAR_ID)
                    .setTimeMin(new DateTime(Date.from(st.toInstant())))
                    .setTimeMax(new DateTime(Date.from(et.toInstant())))
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();
            List<Event> items = events.getItems();
            if (!items.isEmpty()) {
                 numberOfHolidays=countHolidaysOnWeekdays(items)-countWorkdaysOnWeekend(items);
            }
        } catch (IOException e) {
            logger.error("Something went wrong during the Events creation!",e);
        }

        return numberOfHolidays;
    }

    private int countWorkdaysOnWeekend( List<Event> items){
        int workdaysOnWeekend=0;
        for (Event event : items){
            LocalDate localDate = LocalDate.parse(event.getStart().getDate().toStringRfc3339());
            if (localDate.getDayOfWeek() == DayOfWeek.SATURDAY && (event.getSummary().toLowerCase().contains("munkanap") || event.getSummary().toLowerCase().contains("extra work day"))) {
                workdaysOnWeekend++;
            }
        }
        return workdaysOnWeekend;
    }

    private int countHolidaysOnWeekdays(List<Event> items){
        int numberOfHolidays=0;
        for (Event event : items) {
                LocalDate localDate = LocalDate.parse(event.getStart().getDate().toStringRfc3339());
                if (localDate.getDayOfWeek() != DayOfWeek.SATURDAY && localDate.getDayOfWeek() != DayOfWeek.SUNDAY && !isItExceptionEvent(event.getSummary())) {
                    numberOfHolidays++;
                }
        }
        return numberOfHolidays;
    }

    private boolean isItExceptionEvent(String eventSummary){
        for (String event : HOLIDAY_EXCEPTIONS){
            if (eventSummary.toLowerCase().contains(event.toLowerCase())){
                return true;
            }
        }
        return false;
    }

}
