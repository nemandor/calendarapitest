package hu.nemeth;

import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;

public class ListEvents {

    final static Logger logger = Logger.getLogger(ListEvents.class);

    private static final String CALENDAR_ID="hu.hungarian#holiday@group.v.calendar.google.com";
    private static Calendar service = new GoogleCalendarApi().getService();

    public static void main(String[] args){

        try{

            DateTime now = new DateTime(System.currentTimeMillis());

            Events events = service.events().list(CALENDAR_ID)
                    .setMaxResults(10)
                    .setTimeMin(now)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();
            List<Event> items = events.getItems();
            if(items.isEmpty()){
                System.out.println("No upcoming events found");
            }else{
                System.out.println("\nUpcoming events:\n");
                for (Event event : items){
                    DateTime start = event.getStart().getDateTime();
                    if (start==null){
                        start=event.getStart().getDate();
                    }
                    System.out.printf("%s (%s)\n", event.getSummary(), start);
                }
            }
        } catch (IOException e) {
            logger.error("Something went wrong!",e);
        }
    }
}
