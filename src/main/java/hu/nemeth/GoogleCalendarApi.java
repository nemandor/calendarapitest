package hu.nemeth;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.GoogleUtils;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.CalendarScopes;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;

public class GoogleCalendarApi {

    final static Logger logger = Logger.getLogger(GoogleCalendarApi.class);

    private static final String APPLICATION_NAME="Google Calendar API Java Quickstart";
    private static final JacksonFactory JSON_FACTORY=JacksonFactory.getDefaultInstance();
    private static final String TOKENS_DIRECTORY_PATH="tokens";

    private static final List<String> SCOPES= Collections.singletonList(CalendarScopes.CALENDAR_READONLY);
    private static final String CREDENTIALS_FILE_PATH="credentials.json";
    private Calendar service;

    public Calendar getService() {
        return service;
    }

    public GoogleCalendarApi(){
        setEventService();
    }

    public  void setEventService() {

        final NetHttpTransport HTTP_TRANSPORT;

        try {
            HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            /* If you use browse behind a proxy you can reach the Google Calendar API as the following: */
            /*
            HTTP_TRANSPORT = new NetHttpTransport.Builder()
                    .trustCertificates(GoogleUtils.getCertificateTrustStore())
                    .setProxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("proxy-name", Integer.parseInt("port-number"))))
                    .build();
            */
            service = new Calendar.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
                    .setApplicationName(APPLICATION_NAME)
                    .build();
        } catch (GeneralSecurityException e) {
            logger.error("Something went wrong!", e);
        } catch (IOException e) {
            logger.error("Something went wrong!", e);
        } catch (NumberFormatException e) {
            logger.error("Something wrong with the port number.",e);
        }

    }

    private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException{
        File initialFile= new File(CREDENTIALS_FILE_PATH);
        initialFile.createNewFile();
        Reader targetReader = new FileReader(initialFile);
        GoogleClientSecrets clientSecrets=GoogleClientSecrets.load(JSON_FACTORY,targetReader);
        targetReader.close();

        GoogleAuthorizationCodeFlow flow= new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT,JSON_FACTORY,clientSecrets,SCOPES)
                .setDataStoreFactory(new FileDataStoreFactory(new File(TOKENS_DIRECTORY_PATH)))
                .setAccessType("offline")
                .build();
        LocalServerReceiver receiver = new LocalServerReceiver.Builder().setPort(8889).build();
        return new AuthorizationCodeInstalledApp(flow,receiver).authorize("user");
    }
}
