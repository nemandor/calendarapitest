package hu.nemeth;

import java.time.LocalDate;

public class CalculateEndDate {

    public static void main(String[] args){

        String dateInput = "2019-04-30";
        Integer workdays=4;
        LocalDate localDate = LocalDate.parse(dateInput);

        WorkingDayCalculator workingDayCalculator = new WorkingDayCalculator();

        System.out.println("Calculated date: " + workingDayCalculator.getEndDate(localDate,workdays));
    }
}
